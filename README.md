
# Virtual Reality using Oculus 2 for remote control

## Project development

### Requirements

- Unity 3D
- Meta Quest App
- Camera
- Oculus Quest2

### Built With:

- C#
- Python
- OpenCV library

### Description

The purpose of this project is to establish a connection between a drone and the Oculus Quest 2 device, enabling the latter to receive live video streams from external cameras. To achieve this, a client-server communication system was implemented, with the drone serving as the server and a C# script running on the Oculus as the client. The client component of the project has been successfully realized. C# was selected as the programming language due to its compatibility with the Unity platform. During the implementation process, certain challenges were encountered, but these were overcome through testing and demonstration of image display in Unity and connecting the webcam on laptops as a server to transmit snapshots. In the files you will find in this repository, you will have the chance to set LOG, DEBUG and VR global variables to modify the behaviour of the system.

### Server [Python]

The "Server.py" file runs on the drone and creates a socket to transmit images from a camera. Once started, it continuously listens for connection requests. Upon connecting, it begins reading images from the designated camera using the cv2 library and the "cv2.VideoCapture" function, specifying the camera number. It's important to ensure that the correct camera number is being used. In our scenario, the Oculus device initiates and terminates the connection.

### Client [C#]

This script is responsible for video streaming and inherits from the MonoBehaviour class in Unity. The script links to Unity UI components by defining variables for the GameObjects (see Get started with Unity section). 

It includes methods for starting and stopping the video through button presses (TriggerVideo()), receiving the video through a socket connection (receiveVideo()), and displaying the video (concurrent images) on the Oculus/Unity Image Component (Display Image).

The IP address and Port should be set in the Client script.

### ControllerButton [C#]

The "ControllerButton" script is utilized to give the buttons on the Oculus controllers the ability to send HTTP requests to the drone. The script links to Unity UI components by defining variables for the GameObjects. Here you can find the correct way to link the variables with unity components [Linking components](images/Canvas.png)

The button numbers can be found in the Oculus documentation (see below).

In Unity, the "start" method is executed at the start of script execution and the "update" method is called every frame. When using the application with a real drone in the real world, the location data should be retrieved during each call of the "update" method in order to ensure the real data consistency.

### Getting Started With Unity

-   Download Unity
-   Create new project
-   Select 3D template
-   Change the project name and save
-   Search for the Oculus integration online
-   Assets > Oculus
-   Go to File > Build Settings > Add open Scenes> Build
-   In the Project tab > All Prefabs > Search for OVRCameraRig, Drag and drop to add to the scene
-   GameObject > UI > Canvas. Change the Render Mode to World Space [Canvas](images/Canvas.png)
-   Drag and drop UIHelpers from the prefab to the scene [UIHelpers](images/UIHelper.png)
-   Select the UIHelpers > LaserPointer > activate the Line Renderer
-   Add OVR Raycaster component into the Canvas GameObject and select Pointer as LaserPointer. [LaserPointer](images/LaserPointer.png)
-   Add OVR Raycaster component into the Canvas gameobject and select Pointer as LaserPointer. That's it. Now you can add UI components.
- Now, either follow the following instructions to add the UI components or use the method given in the next section "Building UI in Unity"
-   Add image. GameObject > UI > Image
-   Change the Image name to MyImage to match the declared GameObject Variable in the client script. [Image Game Object](images/ImageGameObject.png)
-   Connect the Client (Script) variable MyImage to your Image game object.
-   Add a TextMeshPro Game Object and connect it to the DebugText variable in the Client script. [Image Game Object](images/ImageGameObject.png)
-   Add a button game object. For the onClick() Function choose "Editor and Runtime", your image game object (My Image). [Broadcast button](images/Broadcast.png)
- Add additional buttons and textMeshPro components to imitate the [given scene image](images/Scene.png)
- Connect the Controller button script to the OVRCameraRig according to the [given image](images/OVRCameraRig.png)
- Adjust the buttons onClick() according to the [given example](images/LocalBroadcast.png) The "SetLocal" calls the SetLocal method in the controllerButton script, so check the script to connect the right buttons to the right methods

### Building UI in Unity

As an alternative to manually adding each component, you can download the Oculus_Demo.unity file from the GitHub repository's "unity" directory and add it to the Assets folder in your Unity project. After downloading the Oculus integration, you have two options: either select your sample scene, navigate to "Scene Options", select "Scene Asset", and choose the Oculus_Demo scene, or create a new scene using the Oculus_Demo scene and remove the previous one.

At the end of the process, Oculus_Demo scene should now include: Directional light, canvas with all the necessary components, OVRCameraRig and UIHelpers.

Note: You still need to connect the objects (i.e. the laserpointer) and the scripts and variables.


### Usage - Uploading to Oculus

-   Use this [video](https://www.youtube.com/watch?v=YwFXQeBmxZ4&ab_channel=DilmerValecillos) to familiarize yourself with integrating projects with the Oculus.
-   Download the Meta Quest from your app store on your mobile device
-   Create an account and verify
-   Switch on the Oculus Quest 2 and make sure the Bluetooth on your mobile device is on
-   Pair the devices.
-   Go to settings on the Meta Quest app and turn on developer mode.
-   In the Unity App, click File > Build Settings > Platform > Android
- Click Switch Platform
-   Connect the Oculus to your laptop, in the build settings check if the Oculus device shows in the Run Device tab
- Choose device and run
- On oculus the app will be opened automatically 
- to return to the app after quitting navigate to apps-> unknown sources


### Activity diagrams

In a practical scenario, it is important to continually update the User Interface to monitor the location of the drone based on its coordinates. This is achieved through the utilization of the getLocal_Loc and getGlobal_Loc methods, which are listed under the "Integration with Drone API" section in the Main Features Functions. With every update frame (at a frequency of 50 frames per second [6]), the real values are displayed to the user. This reactive system was implemented as a trade-off between the computational effort required to provide the best possible real-time performance with the drone.


![Every frame update activity](images/UpdateDiagrams.png)
\
Furthermore, to explain how our interaction with the oculus joysticks works, we are attaching the official documentation [7] image describing the different buttons we have available.


![Joystics documentation](images/OculustJoysticks.png)

\
After understood the types of button we can interact with and their code, here is the complete activity diagram describing the user interaction with the project interface.


![Complete activity diagram of the project](images/ButtonsDiagram.png)


## Integration with Drone API

### Set up the environment

To set up the software correctly,  it is mandatory to run it in an environment with **Ubuntu 20.04**. 
The first step is to install  **Python 2.7** and pip.

```
sudo apt-get update && sudo apt-get -y upgrade
sudo apt-get install libapache2-mod-wsgi python-dev build-essential git curl
sudo wget https://bootstrap.pypa.io/pip/2.7/get-pip.py
python2 get-pip.py
```

Once the general-purpose programming language is well installed in your machine (verify with python2 --version), the following libraries with their specified version are needed to be installed.

```
-   Mavproxy == 1.6.4
-   dronekit == 2.9.1
-   Pymavlink == 2.2.10
-   dronekit-sitl == 3.3.0
-   PyYAML == 5.4.1
```

To install them, just use the command.

```pip install <name_package>==<version_number>```

To verify the libraries just installed, use the command.

``` pip show <name_package>```

The system's proper functioning is not guaranteed if the requirements are not satisfied. 

After setting up the environment, a possible example to verify the functionality offered by the system can be found in the README published here [1].

### The entities

Following the "Run example" description of the repository just mentioned [1], we will have four terminals running to test our first application.

#### Drone simulator

The first terminal is a generator of the drone machines. By the command 

```dronekit-sitl copter --instance=1 --home=43.614623417,7.071577868,584,353```

We are specifying the initial position coordinates (where it will take off) and the number of instances. Also, it is possible to select the type of vehicle instance. From the documentation [2], we can find the command

```dronekit-sitl --list```

The output of this command shows the list of all the available vehicles (which can substitute 'copter' in our previously mentioned command).

```
solo-1.2.0
solo-2.0.18
solo-2.0.20
plane-3.3.0
copter-3.3
rover-2.50
```

As soon as this project's testing phase finishes, this drone generator will be substituted by the real machine.

#### MavProxy

In the second terminal, we will find MavProxy. 
MavProxy is defined here[3] as a "command-line based "developer" ground station software," which will be used in our case as a multiplex of a TCP port.

```mavproxy.py --master tcp:127.0.0.1:5770 --out udp:127.0.0.1:14552 --out udp:127.0.0.1:14553```

The first port mentioned - `tcp:127.0.0.1:5770` - is the 'link' we have with the drone, which, during the testing phase, is represented by the drone instance simulator.

MavProxy allows us to broadcast the output of this socket to multiple others. Following the command to the UDP sockets: `udp:127.0.0.1:14552 --out udp:127.0.0.1:14553`.

#### Flask server

Here, the code from the repository [1] is executed using Flask [4]. NB: The flask version is not specified in the "Set up the environment" section, but the **version 1.14** is suggested.

Here the logic of the system is implemented. In the flask-server.py file, we can find the methods which will be used to call from outside through HTTP requests.

##### Main features functions

Here is a list of the function which will be used in our project.

```
| Name of the method | Type | Parameters  |  Return types |
|  ----------------  | ---- | ----------- |  -----------  |
|  getControlMode()  | GET  |     None    |     String    |
|     getSpeed()     | GET  |     None    |     String    |
|   getLocal_Loc()   | GET  |     None    |     String    |
|   getGlobal_Loc()  | GET  |     None    |     String    |
|  setControlMode()  | POST |  JSON array |  Return code  |
|   goto_relative()  | POST |  JSON array |  Return code  |
|      takeOff()     | POST |  JSON array |  Return code  |
|  ----------------  | ---- | ----------- |  -----------  |
```

*NB: It is required by the python server to ask the drone to take off before requesting it to move in different directions.* 

#### [OPTIONAL] Mission Planner

Mission Planner [5] is a real-time interface that can be used to track the status and the location of the drone. It requests input from a port (Created in MavProxy) and prints all the details in a User Interface to improve the user experience.

#### [OPTIONAL] Client

This last terminal is used only in the testing phase of the system before introducing the VR. Once it is registered in the network, Oculus 2.0 will be the client sending the HTTP requests based on the user's movement. 

## Demo
https://clipchamp.com/watch/DrEwtayS7MN

### Useful Link

[1] --> https://gitlab.eurecom.fr/KARIMB/semester-project-drones/-/tree/dev
\
[2] --> https://dronekit-python.readthedocs.io/en/latest/develop/sitl_setup.html
\
[3] --> https://ardupilot.org/mavproxy/
\
[4] --> https://flask.palletsprojects.com/en/2.2.x/
\
[5] --> https://ardupilot.org/planner/
\
[6] --> https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html
\
[7] --> https://developer.oculus.com/documentation/unity/unity-ovrinput/
