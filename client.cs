using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

using TMPro;

public class client : MonoBehaviour
{

    private bool DEBUG = false;
    private bool VR = true;
    private bool LOG = false;
    
    Texture2D tex;
    private Sprite img1;

    public GameObject MyImage;
    public GameObject DebugText;
    public GameObject IP_address;
    public GameObject Port;

    Socket receiver;
    bool endConnection = false;

    public string data = null;
    public string data_all = "";
    
    public int lenght_message_to_read = 0;
    public byte[] bytes = new Byte[4096];
    public byte[] message;
    
    public string image = null;
    byte[] pngBytes;
    public bool imageChanged = false;
    public bool videoStreaming = false;
    IPHostEntry ipHost = null;
    IPEndPoint localEndPoint = null; 
    public string IPADDR = "192.168.1.35";
    public int PORT = 55000;

    int numByte;

    void Start()
    {
        if (DEBUG)
            Debug.Log("Starting...");
        else if (VR)
            DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Starting...";
            IP_address.GetComponent<TMPro.TextMeshProUGUI>().text = IPADDR;
            Port.GetComponent<TMPro.TextMeshProUGUI>().text = PORT.ToString();
    }

    /* 
      This sentence describes the process of establishing a socket connection,
      sending a socket request, receiving the streams, and waiting for an image transmission.
      When an image is received, the "displayImage" method is then executed.
    */
    public IEnumerator receiveVideo(){
        int count = 0;

        while(true){
            //get message
            try{
                numByte = receiver.Receive(bytes);
            }catch(Exception socket){
                if (DEBUG)
                    Debug.Log("Closed Socket");
                else if (VR)
                    DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Closed Socket";
                break;
            }
            data = Encoding.ASCII.GetString(bytes, 0, numByte);

            if (DEBUG)
                Debug.Log("Received Data" + data);
            else if (VR)
                DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Received Data";

            if(count >0){
                try{
                    Regex reg1 = new Regex("^[9]+");
                    data = reg1.Replace(data, "");

                    if (DEBUG)
                        Debug.Log("data: " + data);
                }catch(Exception e) {
                    if (DEBUG)
                        Debug.Log("Error removing leading 9's: " + e);
                }
            }
            count += 1;

            if(data.Length>=3){
                string checkEnd = data.Substring(data.Length-3, 3);

                if(checkEnd == "999"){
                    if (DEBUG)
                        Debug.Log("Image end");

                    try{
                        char last_char = data[data.Length - 1];
                        while(last_char == '9'){
                            data = data.Substring(0, data.Length-1);
                            last_char = data[data.Length - 1];
                        }
                        data_all += data;
                    }catch(Exception e){
                        if (DEBUG)
                            Debug.Log(e);
                    }

                    image = data_all;

                    if (DEBUG)
                        Debug.Log("Update image");
                    else if (VR)
                        DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Update Image";
                        
                    StartCoroutine( DisplayImage(image));
                    yield return null;

                    data = null;
                    data_all = "";
                    
                    lenght_message_to_read = 0;
                    
                }else if(checkEnd == "888"){
                    if (DEBUG)
                        Debug.Log("Code to end just received, connection is shutting down...");

                    data = null;
                    data_all = "";
                    
                    lenght_message_to_read = 0;
                    
                    receiver.Shutdown(SocketShutdown.Both);
                    receiver.Close();

                    endConnection = true;
                    yield return null;
                }
                else{
                    if (data != null || data!="" && data.Length >0){
                       
                        try{
                            char last_char = data[data.Length - 1];
                            data_all += data;
                        }catch(Exception e){
                            if (DEBUG)
                                Debug.Log("Data Length: " + e);
                        }
                    }
                }
            }
        }
    }

   /*
    When the user presses the 'Video' button, the UI sends out a broadcast that activates this method.
    If the video is not currently streaming, the method will invoke the "receive Video" method, establishing a socket connection.
   */
    void TriggerVideo(){
        if (DEBUG)
            Debug.Log("Broadcast received");
        else if (VR)
            DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Broadcast received";
        
        if (!videoStreaming){
            if (DEBUG)
                Debug.Log("Starting client...");
            else if (VR)
                DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Starting Client";
                
            try {
                // Establish the remote endpoint for the socket. 
                ipHost = Dns.GetHostEntry(Dns.GetHostName());
                localEndPoint = new IPEndPoint(IPAddress.Parse(IPADDR), PORT); 

                // Creation TCP/IP Socket using
                // ipAddr.AddressFamily --> Essential to connect in localhost
                // https://learn.microsoft.com/en-us/dotnet/api/system.net.ipaddress.addressfamily?view=net-7.0
                receiver = new Socket(IPAddress.Parse(IPADDR).AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                if (DEBUG)
                    Debug.Log("Socket created!");
                else if (VR)
                    DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Socket created";

            }catch(Exception econn){
                if (DEBUG)
                        Debug.Log("Socket error " + econn);
                else if (VR)
                    DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Socket error" + econn;
                return;
            }

            if (DEBUG)
                Debug.Log("Start streaming");
            else if (VR)
                DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Start streaming";
            try {
                    // Connect Socket to the remote endpoint using method Connect()
                    receiver.Connect(localEndPoint);

                    if (DEBUG)
                        Debug.Log("Client has connected, starting now to generate data flow!");
                    else if (VR)
                        DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Client has connected, starting now to generate data flow!";


                    //Execute in parallel
                    StartCoroutine(receiveVideo());

                }catch(Exception esocket){
                    if (DEBUG)
                        Debug.Log("Connection error" + esocket);
                    else if (VR)
                        DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Connection error" + esocket;
                    return;
                }
            videoStreaming = true;

        }else{
            if (DEBUG)
            Debug.Log("Stop streaming");
            else if (VR)
            DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Stop streaming";    

            receiver.Shutdown(SocketShutdown.Both);
            receiver.Close();

            data = null;
            data_all = "";
            lenght_message_to_read = 0;

            videoStreaming = false;
        }
    }

    //In development: Testing setting socket from UI
    void SetIP(){
        if (DEBUG)
            Debug.Log("SetIP");
        else if(VR)
            DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Set IP";
        
        string Port_input = Port.GetComponent<TMPro.TextMeshProUGUI>().text;
        string IP_input = IP_address.GetComponent<TMPro.TextMeshProUGUI>().text;

        try{

            string[] ipStringArr = IP_input.Split('.');
            int[] ipIntArr = new int[ipStringArr.Length];
            if (ipIntArr.Length != 4)
            {
                if (DEBUG)
                    Debug.Log("IP in wrong format");
                else if(VR)
                    DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "IP wrong format";
            }
            else
            {
                for (int i = 0; i < ipStringArr.Length; i++)
                {
                    ipIntArr[i] = Int32.Parse(ipStringArr[i]);
                    if (ipIntArr[i] < 0 || ipIntArr[i] > 255)
                    {
                    if (DEBUG)
                        Debug.Log("IP in wrong format");
                        else if(VR)
                            DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "IP wrong format";
                    }
                    else{
                        IPADDR = IP_input;
                        PORT =  Convert.ToInt32(Port_input);
                        DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "Set IP: " + IPADDR + "\nPort: " + PORT.ToString();

                    }
                }
            }
        }catch(Exception except){
            if (DEBUG)
                    Debug.Log("IP " + except);
            else if(VR)
                DebugText.GetComponent<TMPro.TextMeshProUGUI>().text = "IP " + except;

        }
    }
    
    /*
       The README.md file's "Getting Started" section explains how to create a component in Unity
       and link script variables to UI components. The method inputs a string stream and displays
       the image in the game component in Unity through the use of the Convert method.
    */
    public IEnumerator DisplayImage(string image)
    {
        bool error = false;

        try{
            pngBytes = Convert.FromBase64String(image);
        }catch(Exception e){
            image = Regex.Replace(image,"[^a-zA-Z/+]", "");
            if (DEBUG)
                    Debug.Log("Error Displaying image: " + e);
            try{
                pngBytes = Convert.FromBase64String(image);
            }catch(Exception ex){
                if (DEBUG)
                    Debug.Log("Error Displaying image: " + ex);
                error = true;
                //return;
            }
        }

        if(error)
            yield return null;

        //Load data into the texture.
        else{
            tex = new Texture2D(1, 1);
            tex.LoadImage(pngBytes);
            if(DEBUG)
                Debug.Log("Texture loaded");

            if(tex.width <= 20){
                yield return null;
            }
            else{
            
                // Assign texture to renderer's material.
                img1  = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(1.0f, 1.0f), 100.0f);

                if(DEBUG)
                    Debug.Log("Sprite created");

                // Accessing MyImage component 
                MyImage.GetComponent<UnityEngine.UI.Image>().sprite = img1;

                if(DEBUG)
                    Debug.Log("MyImage loaded");

                yield return null;
            }
        }
    }
}