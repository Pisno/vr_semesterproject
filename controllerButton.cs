using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OVRTouchSample;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net.Http;
using static System.Net.Http.HttpMessageInvoker;
using System.Text;
using System;
using System.Text.RegularExpressions;


public class controllerButton : MonoBehaviour{
    public GameObject DebugController;

    public GameObject LocalText;
    public GameObject GlobalText;
    public GameObject ControlText;
    public GameObject SpeedText;
   
    // Start is called before the first frame update

    string getControlMode = "http://192.168.1.98:5050/getControlMode";
    string set = "http://192.168.1.98:5050/setControlMode";
    string getLocal = "http://192.168.1.98:5050/getLocal_Loc";
    string getGlobal = "http://192.168.1.98:5050/getGlobal_Loc";
    string takeOff = "http://192.168.1.98:5050/takeOff/200";
    string move = "http://192.168.1.98:5050/goto_relative";
    string getSpeed = "http://192.168.1.98:5050/getSpeed"; 

    string north = "0";
    string east = "0";
    string down = "0"; 
    //const speed
    float speedV = 200f; 
    string speed = "200"; 

    float increase = 10f;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.Get(OVRInput.Button.One)){
            DebugController.GetComponent<TMPro.TextMeshProUGUI>().text = "settings";
        } 

        //increase north value
        else if (OVRInput.Get(OVRInput.Button.Two)){
            DebugController.GetComponent<TMPro.TextMeshProUGUI>().text = "move north";
            MoveHttpReq("north");
        }

        //increase east value 
        else if (OVRInput.Get(OVRInput.Button.Three)){
            DebugController.GetComponent<TMPro.TextMeshProUGUI>().text = "move east";
            MoveHttpReq("east");}

        //increase down value
        else if (OVRInput.Get(OVRInput.Button.Four)){
            DebugController.GetComponent<TMPro.TextMeshProUGUI>().text = "move down";
            MoveHttpReq("down");}
        
        //should be uncommented when running in real world
        /*else {
           Get("local"); 
        }*/
    }

    async Task MoveHttpReq(string type){
        try{
            string myJson = "";

            if (type == "north"){
                float northV = float.Parse(north) + increase;
                MyNorth myObject = new MyNorth();
                myObject.dNorth = northV;
                myObject.speed= speedV;
                myJson = JsonUtility.ToJson(myObject);
            }else if (type == "east"){
                float eastV = float.Parse(east) + increase;
                MyEast myObject = new MyEast();
                myObject.dEast = eastV;
                myObject.speed= speedV;
                myJson = JsonUtility.ToJson(myObject);
            }else if (type == "down"){
                float downV = float.Parse(down) + increase;
                MyDown myObject = new MyDown();
                myObject.dDown = downV;
                myObject.speed= speedV;
                myJson = JsonUtility.ToJson(myObject);
            }

            DebugController.GetComponent<TMPro.TextMeshProUGUI>().text = "Move: " + myJson.ToString();

            using (var client = new System.Net.Http.HttpClient()){
                var response = await client.PostAsync(move, new StringContent(myJson, Encoding.UTF8, "application/json")).ConfigureAwait(false);
            }

        }catch(Exception e){
            DebugController.GetComponent<TMPro.TextMeshProUGUI>().text = "Move " + e;
        }
    }
    
    async Task GetHttpReq(string type){
        var client = new System.Net.Http.HttpClient();
        var req = "";
        if (type == "control"){
            req = getControlMode;
        }
        var result = await client.GetAsync(req);
        var stat = (int)result.StatusCode;
        var content = await result.Content.ReadAsStringAsync();
        ControlText.GetComponent<TMPro.TextMeshProUGUI>().text = content;
    }

    async Task Get(string type){
        var client = new System.Net.Http.HttpClient();
        var req = "";
        if (type == "local"){
            req = getLocal;
        }else if (type == "global"){
            req = getGlobal;}

        HttpResponseMessage result = await client.GetAsync(req);
        var stat = (int)result.StatusCode;
        if (stat == 200){
            var content = await result.Content.ReadAsStringAsync();
            content = content.Substring(14,content.Length-14);
            char[] separator = {',', '='};

            string[] strlist = content.Split(separator);
            north = strlist[1];
            east = strlist[3];
            down = strlist[5];
            try{
                north = north.Substring(0,6);
                east = north.Substring(0,6);
                down = down.Substring(0,6);
            }catch(Exception ex){
            }
            if(type == "local"){
                LocalText.GetComponent<TMPro.TextMeshProUGUI>().text = "North: " + north + " East: " +east + " Down: " +down;
            }else if (type == "global"){
                GlobalText.GetComponent<TMPro.TextMeshProUGUI>().text = "North: " + north + " East: " +east + " Down: " +down;
            }
        }
    
    }

    async Task GetSpeed(){
        var client = new System.Net.Http.HttpClient();
        var req = getSpeed;
        HttpResponseMessage result = await client.GetAsync(req);
        var stat = (int)result.StatusCode;
        if (stat == 200){
            var content = await result.Content.ReadAsStringAsync();
            speed = float.Parse(content).ToString("#.##");

            SpeedText.GetComponent<TMPro.TextMeshProUGUI>().text ="Speed " + speed.ToString();
        }
    
    }

    async Task PostHttpReq(string type){
        using (var client = new System.Net.Http.HttpClient()){
            var req = "";
            if (type == "takeOff"){
                req = takeOff;
            }
            var result = await client.PostAsync(req, new StringContent("{}", Encoding.UTF8, "application/json")).ConfigureAwait(false);
        }
    }

    async Task SetModeReq(){
        string myJson = "{\"mode\":\"GUIDED\"}";
        using (var client = new HttpClient())
        {
            var response = await client.PostAsync(set , 
                new StringContent(myJson, Encoding.UTF8, "application/json"));

            var stat = (int)response.StatusCode;
            DebugController.GetComponent<TMPro.TextMeshProUGUI>().text = stat.ToString();
        }
    }

    void SetGlobal(){
        GlobalText.GetComponent<TMPro.TextMeshProUGUI>().text = "Buffer";
        Get("global");
    }

    void SetLocal(){
        LocalText.GetComponent<TMPro.TextMeshProUGUI>().text = "Buffer";
        Get("local");
    }

    void SetControl(){
        ControlText.GetComponent<TMPro.TextMeshProUGUI>().text = "Buffer";
        GetHttpReq("control");
    }
    void SetSpeed(){
        SpeedText.GetComponent<TMPro.TextMeshProUGUI>().text ="Buffer ";
        GetSpeed();
    }
    void TakeOff(){
        DebugController.GetComponent<TMPro.TextMeshProUGUI>().text = "Take off";
        PostHttpReq("takeOff");
    }
    void SetMode(){
        DebugController.GetComponent<TMPro.TextMeshProUGUI>().text = "Set Mode";
        SetModeReq();
    }
}

[Serializable]
public class MyNorth{
    public float dNorth;
    public float speed;
}

[Serializable]
public class MyEast{
    public float dEast;
    public float speed;
}

[Serializable]
public class MyDown{
    public float dDown;
    public float speed;
}
