import base64
import socket
import sys
from cv2 import cv2
import time
from threading import Thread

HOST = ''
#PORT = 0 use this if you want the port to be random picked based on OS availability
PORT = 55000
sleeping_time = 0.04
DEBUG = True
LOG = True

if len(sys.argv)>1 and sys.argv[1] != None and sys.argv[1] == "-debug":
    DEBUG = True

if len(sys.argv)>2 and sys.argv[2] != None and sys.argv[2] == "-log":
    LOG = True

while True:

    try:
        '''
         Creation of socket; first of all the binding given the missing information of host and port, which cause the
         connection to link immediatly to localhost (in this case), meanwhile the port is randomly picked based on the first one which is free
        '''
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((HOST, PORT))
        s.listen(10)

        print('Socket bind complete to port: ' + str(s.getsockname()[1]))

        if DEBUG:
            print('Socket created')
            print('Socket now listening')

        # [SECURITY BREACH] Whenever there is a request, the server accepts the client
        conn, addr = s.accept()
        if DEBUG:
            print('Socket accepted connection')

        # Stream the video taken from main camera of the pc, for other type of cameras, such as USB's ones, use 1 or check your OS details
        cap = cv2.VideoCapture(0)

        while True:

            retval, image = cap.read()
            retval, buffer = cv2.imencode('.jpg', image)
            converted_image = base64.b64encode(buffer)

            if LOG:
                with open('encoded-image.txt', 'w') as f:
                    f.write(str(converted_image))

            if DEBUG:
                print("Len image:" +str(len(converted_image)))

            '''
              For each iteration in the range object defined by 
              the starting value of 0, the end value of len(converted_image), and the step of 4096
              Get a slice of the binary string 'converted_image' starting from 'i' and of length 4096
            '''
            converted_image_split = [converted_image[i:i+4096] for i in range(0, len(converted_image), 4096)] 

            # after connecting, we send to the client all the chunks
            for chunk in converted_image_split:
                conn.sendall(chunk)

            '''
             We had to find a way to interrupt the connection as soon as the stream ends
             For this reason we have created a unique code (really unlikely that is going to be replicated in the payload, '9' in a row).
             If len(chunk)
            '''
            if LOG:
                print('end image: '+ str(len(chunk)))

            # Check if the length of the binary string 'chunk' is less than 4090
            if(len(chunk) < 4090):
                
                '''
                  If the length of 'chunk' is less than 4090
                  send the remaining bytes to make it 4096 in total by concatenating
                  the chunk with the string containing '9' repeated (4096 - len(chunk)) times
                '''
                conn.sendall(("9"*(4096-len(chunk))).encode())
                
            else:
                '''
                  Otherwise send a string of 4096 bytes containing only the character '9'.
                  By using this approach, we can detect the next image coming from the stream.
                '''
                conn.sendall(("9"*(4096)).encode())

            '''
             By tests we understood that 0.04 value is a good trade-off between the computation of Unity/Oculus
             and the smoothness of the video streaming. Obviously, this value can change for each machine
             based on your computation resources availabile.  
            '''
            time.sleep(sleeping_time)

        if DEBUG:
            print("Sent everything regarding the image!")
            print('Code to end just sent, connection is shutting down...')

        '''
         Socket is closed.
         When you call close it decrements the handle count by one and if the handle count has reached zero then the socket
         and associated connection goes through the normal close procedure (effectively sending a FIN / EOF to the peer)
         and the socket is deallocated. We need to deallocate the socket so .shutdown() is not usable!
        '''
        s.close()

        if DEBUG:
            print("Socket closed")

    except:
        print("Client closed connection")
        time.sleep(1)

